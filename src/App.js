import React from 'react';
import Header from './components/Header';
import Body from './components/Body';
import './App.css';

class App extends React.Component{

  state={
    user_id: 13323,
    first_name: 'Shubham',
    last_name: 'Chakraborty',
    username: 'armostheus'
  }

  render(){
    return (
      <div className="App">
        <Header 
          name={this.state.first_name+' '+this.state.last_name}
          user_id={this.state.user_id}
          username={this.state.username}
        />
  
        <Body />
      </div>
    );
  }
}


export default App;
