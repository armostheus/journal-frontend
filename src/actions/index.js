export const setSelectedDate = (date = null) => (dispatch, getState) => {
    let thisDate = new Date();
    if(date){
       thisDate=date; 
    }
    dispatch({type:'SET_DATE', payload: thisDate});
}

export const ViewEntry = (date) => async (dispatch, getState) => {
    await dispatch(fetchEntries(date));
}

export const ViewGoals = ( date ) => async (dispatch,getState) => {
    await dispatch(fetchGoals(date));
}

export const ViewLesson = ( date ) => async (dispatch,getState) => {
    await dispatch(fetchLesson(date));
}

export const ViewSummary = ( date ) => async (dispatch,getState) => {
    await dispatch(fetchSummary(date));
}