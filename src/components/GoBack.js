import React from 'react'

function GoBack(props){
    return(
        <div >
            <p>Data Successfully Submitted</p>
            <button type="button" class="btn btn-primary" onClick={props.onClick}>Bo Back</button>
        </div>
    )
}

export default GoBack;