import React from 'react';

class InputGoals extends React.Component {
    constructor(){
        super();
        this.state = {
            goal:'',
            priority:5,
            editable:true,
            startDate:null,
            endDate:null
        }
        this.changeGoal = this.changeGoal.bind(this);
        this.changePriority = this.changePriority.bind(this);
        this.changeEditable = this.changeEditable.bind(this);
        this.changeStartDate = this.changeStartDate.bind(this);
        this.changeEndDate = this.changeEndDate.bind(this);
        this.buildAndSubmitData = this.buildAndSubmitData.bind(this);
    }

    changeGoal(e){
        this.setState({goal:e.target.value});
    }

    changePriority(e){
        this.setState({priority:e.target.value});
    }
    changeEditable(e){
        this.setState({editable:e.target.value});
    }
    changeStartDate(e){
        this.setState({startDate:e.target.value});
    }
    changeEndDate(e){
        this.setState({endDate:e.target.value});
    }
    buildAndSubmitData(){
        console.log(JSON.stringify(this.state));
    }
    

    render (){
        return(
            <div className="container-fluid" style={{border:'solid'}}>
                <br/>
                <div className="row">
                    <label className="col-sm-3">Goal</label>
                    <input className="col-sm-9 form-control" type="text" placeholder="Enter your goal" onChange={this.changeGoal}></input>
                </div>
                <br/>
                <div className="row">
                    <label className="col-sm-3">Priority</label>
                    <select class="form-control col-sm-9" onChange = {this.changePriority}>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select>
                </div>
                <br/>
                <div className="row">
                    <label className="col-sm-3">Editable</label>
                    <select class="form-control col-sm-9" onChange={this.changeEditable}>
                        <option>Yes</option>
                        <option>No</option>
                    </select>
                </div>
                <br/>
                <div className="row">
                    <label className="col-sm-3">Start Date</label>
                    <input type="date" name="bday" max="3000-12-31" min="1000-01-01" className="form-control col-sm-9" onChange={this.changeStartDate}></input>
                </div>
                <br/>
                <div className="row">
                    <label className="col-sm-3">End Date</label>
                    <input type="date" name="bday" max="3000-12-31" min="1000-01-01" className="form-control col-sm-9" onChange={this.changeEndDate}></input>
                </div><br/>
                <div>
                    <button type="button" class="btn btn-primary" onClick={this.buildAndSubmitData}>Submit</button>
                </div>
            </div>
        )
    }
}

export default InputGoals;