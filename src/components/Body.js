import React, { Component } from 'react';
import CalendarElement from './Calendar'
import InputElement from './InputElement'
import ViewElement from './ViewElement'

class Body extends Component {
    constructor(props){
        super(props);
        this.state={
            selectedDate: new Date(),
            updateView: false
        }
        this.handleCalendarChange = this.handleCalendarChange.bind(this);
    }

    handleCalendarChange(date){
        console.log(date);
        this.setState({selectedDate:date});
    }

    reloadEntryView(){
        this.setState({updateView:true});
        console.log('reload entry view to '+this.state.updateView);
        this.setState({updateView: false})
    }

    render(){
        return (
            <div className="container-fluid">
                <div className='row'>
                    <div className="col-sm-4">
                        <CalendarElement onChange={this.handleCalendarChange}/>
                    </div>
                    <div className="col-sm-4">
                        <InputElement selectedDate={this.state.selectedDate} reloadEntryView={()=>this.reloadEntryView()}/>
                    </div>
                    <div className="col-sm-4">
                        <ViewElement selectedDate={this.state.selectedDate} reloadEntryView={this.state.updateView}/>
                    </div>
                </div>
            </div>    
        );
    }
}

export default Body;