import React from 'react';

class InputLesson extends React.Component {
    constructor(props){
        super(props);
        console.log(props.summary_Id);
        this.state = {
            summaryId:props.summaryId,
            lesson:''
        }
        this.changeLesson = this.changeLesson.bind(this);
        this.buildAndSubmitData = this.buildAndSubmitData.bind(this);
    }

    changeLesson(e){
        this.setState({lesson:e.target.value})
    }

    buildAndSubmitData(){
        console.log(JSON.stringify(this.state))
    }

    render(){
        let summId = this.props.summary_Id;
        if(summId===0 || summId===null){
            return(
                <div className="container-fluid" style={{border:'solid'}}>
                    Please Enter Summary of the day first.
                </div>
            )
        }else{
            return(
                <div className="container-fluid" style={{border:'solid'}}>
                    <br/>
                    <div className="row">
                        <label className="col-sm-3">Lesson</label>
                        <input className="col-sm-9 form-control" type="text" placeholder="Summary title." onChange={this.changeLesson}></input>
                    </div><br/>
                    <div>
                        <button type="button" class="btn btn-primary" onClick={this.buildAndSubmitData}>Submit</button>
                    </div>
                </div>
            )
        }
        
    }
}

export default InputLesson;