import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import ViewEntry from './ViewEntry';
import ViewGoals from './ViewGoals';
import ViewLesson from './ViewLesson';
import ViewSummary from './ViewSummary';
import axios from 'axios';

class ViewElement extends Component {
    constructor(props){
        super(props);
        this.state={
            date: props.selectedDate,
            updateEntryView: false,
            testData:'',
            entryData:'',
            summaryData:'',
            lessonsData:'',
            goalsData:''
        }
    }

    componentDidMount(){
        //make get request to fetch data from db
        console.log('view component mounted');
        axios.get(`http://localhost:3100/dbApi/testRoute`)
        .then(res => {
          const persons = res.data;
          this.setState({ testData: persons });
          console.log(res.data);
        });
        this.setState({
            date: this.props.selectedDate          
        })
    }

    componentDidUpdate(){
        console.log('view component updated');
        if(this.state.date!==this.props.selectedDate){
            this.setState({
                date: this.props.selectedDate
            })
        }
        if(this.state.updateEntryView!==this.props.reloadEntryView){
            this.setState({
                updateEntryView:this.props.reloadEntryView
            });    
        }
        
    }

    render(){
        return (
            <Router>
            <div className="container-fluid row">
                <div>{this.props.selectedDate + ' ... ' + this.state.testData}</div>
                <div className="list-group list-group-horizontal">
                    <Link to="/ViewDailyEntry" className="list-group-item list-group-item-action active">Entry</Link>
                    <Link to="/ViewSummary" className="list-group-item list-group-item-action">Summary</Link>
                    <Link to="/ViewLessons" className="list-group-item list-group-item-action">Lesson</Link>
                    <Link to="/ViewSetGoals" className="list-group-item list-group-item-action">Goals</Link>
                </div>
                <div>
                    <Route 
                        path="/ViewDailyEntry"
                        render={(props)=><ViewEntry {...props} searchDate={this.state.date} reloadEntryView={this.state.updateEntryView}/>} 
                    />
                    <Route 
                        path="/ViewSetGoals" 
                        render={(props)=><ViewGoals {...props} searchData={this.state.goals} />}
                    />
                    <Route 
                        path="/ViewLessons" 
                        render={(props)=> <ViewLesson {...props} searchData={this.state.lessonsData} />} 
                    />
                    <Route 
                        path="/ViewSummary" 
                        render={(props)=><ViewSummary {...props} searchData={this.state.summaryData} />} 
                    />
                </div>
            </div> 
            </Router> 
        );
    }
}

export default ViewElement;