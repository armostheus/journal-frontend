import React from 'react';
import axios from 'axios';

class ViewEntry extends React.Component{
    constructor(){
        super();
        this.state={
            entries:{}
        }
    }

    componentDidMount(){
        axios.get('http://localhost:3100/dbAPI/logEntry/'+this.props.searchDate.getDate()+'-'+(this.props.searchDate.getMonth()+1)+'-'+this.props.searchDate.getFullYear()+'.'+this.props.searchDate.getDate()+'-'+(this.props.searchDate.getMonth()+1)+'-'+this.props.searchDate.getFullYear())
        .then(res => {
            this.setState({entries:res});
            console.log(res);
        })
        .catch(err => console.log(err)); 
        console.log(this.props.reloadEntryView);
        console.log(this.props.searchDate);
    }

    componentDidUpdate(){
        console.log(this.props.reloadEntryView);
        console.log(this.props.searchDate);
    }

    render(){
        return(
            <div className="container-fluid row" >
                View Entry 
            </div>
        )
    }
}

export default ViewEntry;