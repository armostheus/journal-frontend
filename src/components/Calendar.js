import React, { Component } from 'react';
import Calendar from 'react-calendar';

class CalendarElement extends Component {

    state = {
        date: new Date()
    }

    onChange = date => {
        this.props.onChange(date);
        this.setState({date})
    }

    render(){
        return (
            <div className="container-fluid">
                <Calendar
                    onChange={this.onChange}
                    value={this.state.date} 
                />
            </div>    
        );
    }
}

export default CalendarElement;