import React from 'react';
import GoBack from './GoBack';
import axios from 'axios';

class InputEntry extends React.Component{
    constructor(){
        super();

        this.state = {
            title:'',
            desc:'',
            mood:'',
            star: false,
            sad: false,
            media: null,
            mediaCaption:'',
            submitted:false,
            disabledView:false
        }

        this.buildData = this.buildData.bind(this);
        this.changeTitle = this.changeTitle.bind(this);
        this.changeDesc = this.changeDesc.bind(this);
        this.changeMood = this.changeMood.bind(this);
        this.starClicked = this.starClicked.bind(this);
        this.sadClicked = this.sadClicked.bind(this);
        this.comeBack = this.comeBack.bind(this);
    }

    buildData(){
        let entry = {
            userUserId:1223,
            title: this.state.title,
            description: this.state.desc,
            mood: this.state.mood,
            star: this.state.star,
            sad: this.state.sad,
            media: this.state.media,
            mmedia_caption: this.state.mediaCaption
        }
        this.setState({disabledView:true});        
        console.log(JSON.stringify(entry)+"YOLO");
        axios.post('http://localhost:3100/dbAPI/logEntry/', entry)
        .then(res => {
            this.setState({disabledView:false});
            this.setState({submitted:true});
            console.log(res);
            this.props.reloadEntryView();
        })
        .catch(err => console.log(err));  
    }

    changeTitle(e){
        this.setState({title:e.target.value});
    }
    changeDesc(e){
        this.setState({desc:e.target.value});
    }
    changeMood(e){
        this.setState({mood:e.target.value});
    }
    starClicked(){
        this.setState((prevState) => ({star:!prevState.star}))
    }
    sadClicked(){
        this.setState((prevState) => ({sad:!prevState.sad}))
    }
    comeBack(){
        this.setState({submitted:false});
        this.props.reloadEntryView();
    }
    render(){

        if(this.state.submitted){
            return(<GoBack onClick={this.comeBack} />);
        }else{
            return(
                <div className="container-fluid" id="outer-container" style={this.state.disabledView ? {"pointer-events": "none", "opacity": "0.4"}:{}}>
                    <br/>
                    <div className="row">
                        <label className="col-sm-3">Title</label>
                        <input className="col-sm-9 form-control" type="text" placeholder="Title for your entry" onChange={this.changeTitle}></input>
                    </div>
                    <br/>
                    <div className="row">
                        <label for="exampleFormControlTextarea1" className="col-sm-3">Desc</label>
                        <textarea className="form-control col-sm-9" id="exampleFormControlTextarea1" rows="3"  onChange={this.changeDesc}></textarea>
                    </div>
                    <br/>
                    <div className="row">
                        <label className="col-sm-3">Mood</label>
                        <select class="form-control col-sm-9"  onChange={this.changeMood}>
                            <option value='1'>Happy</option>
                            <option value='2'>Ecstatic</option>
                            <option value='3'>Joyful</option>
                            <option value='4'>Fearful</option>
                            <option value='5'>Afraid</option>
                            <option value='6'>Amazing</option>
                            <option value='7'>Depressed</option>
                            <option value='8'>Wrong</option>
                            <option value='9'>Bored</option>
                            <option value='10'>Interesting</option>
                            <option value='11'>Annoyed</option>
                            <option value='12'>Angry</option>                        
                            <option value='13'>Trustworthy</option>
                            <option value='14'>Disgusted</option>
                            <option value='15'>Betrayed</option>
                            <option value='16'>Grieved</option>
                            <option value='17'>Sad</option>
                            <option value='18'>Surprised</option>
                            <option value='19'>Peaceful</option>
                            <option value='20'>Lonely</option>
                            <option value='21'>Enlightened</option>
                            <option value='22'>Alone</option>
                        </select>
                    </div>
                    <br/>
                    <div className="row">
                        <label className="col-sm-3"></label>
                        <label className="col-sm-4" onClick={this.starClicked}>Star Icon</label>
                        <label className="col-sm-4" onClick={this.sadClicked}>Sad Icon</label>
                    </div>
                    <br/>
                    <div className="row">
                        <label className="col-sm-3">Media</label>
                        <input className="col-sm-9 form-control" type="text" placeholder="Title for your entry"></input>
                    </div>
                    <br/>
                    <div className="row">
                        <label className="col-sm-3">Caption</label>
                        <input className="col-sm-9 form-control" type="text" placeholder="Title for your entry"></input>
                    </div><br/>
                    <div>
                        <button type="button" class="btn btn-primary" onClick={this.buildData}>Submit</button>
                    </div>
    
                </div>
            )
        }    
    }
}

export default InputEntry;