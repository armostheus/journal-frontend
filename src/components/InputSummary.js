import React from 'react';

class InputSummary extends React.Component {
    constructor(){
        super();
        this.state = {
            title:'',
            summary:'',
            howWasDay:'',
            media:'',
            mediaCaption:''
        }

        this.changeTitle = this.changeTitle.bind(this);
        this.changeSummary = this.changeSummary.bind(this);
        this.changeHowWasDay = this.changeHowWasDay.bind(this);
        this.buildAndSubmitData = this.buildAndSubmitData.bind(this);
    }

    changeTitle(e){
        this.setState({title:e.target.value});
    }

    changeSummary(e){
        this.setState({summary:e.target.value});
    }

    changeHowWasDay(e){
        this.setState({howWasDay:e.target.value});
    }

    buildAndSubmitData(){
        console.log(JSON.stringify(this.state));
    }

    render(){
        return(
            <div className="container-fluid" style={{border:'solid'}}>
                <br/>
                <div className="row">
                    <label className="col-sm-3">Title</label>
                    <input className="col-sm-9 form-control" type="text" placeholder="Summary title." onChange={this.changeTitle}></input>
                </div>
                <br></br>
                <div className="row">
                    <label for="exampleFormControlTextarea1" className="col-sm-3">Summary</label>
                    <textarea className="form-control col-sm-9" id="exampleFormControlTextarea1" rows="3" onChange={this.changeSummary}></textarea>
                </div>
                <br/>
                <div className="row">
                    <label for="exampleFormControlTextarea1" className="col-sm-3">How was day</label>
                    <select class="form-control col-sm-9" onChange={this.changeHowWasDay}>
                        <option>Awesome</option>
                        <option>Great</option>
                        <option>Good</option>
                        <option>Bad</option>
                        <option>Really Bad</option>
                    </select>
                </div>
                <br/>
                <div className="row">
                    <label for="exampleFormControlTextarea1" className="col-sm-3">Media</label>
                    <input className="col-sm-9 form-control" type="text" placeholder="Summary title."></input>
                </div>
                <br/>
                <div className="row">
                    <label for="exampleFormControlTextarea1" className="col-sm-3">Caption</label>
                    <input className="col-sm-9 form-control" type="text" placeholder="Summary title."></input>
                </div><br/>
                <div>
                    <button type="button" class="btn btn-primary" onClick={this.buildAndSubmitData}>Submit</button>
                </div>
            </div>
        )
    }
}

export default InputSummary;