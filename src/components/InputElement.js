import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import InputEntry from './InputEntry';
import InputGoals from './InputGoals';
import InputLesson from './InputLesson';
import InputSummary from './InputSummary';

class InputElement extends Component {
    constructor(props) {
        super(props);
        console.log(props.selectedDate);
        this.state = {
            date: this.props.selectedDate ,
            entryStatus: '',
            summaryStatus: '',
            lessonStatus: 'active',
            goalStatus: '',
            summaryId: 0
        };
        this.activateButton = this.activateButton.bind(this);
    }

    componentDidMount(){
        //search for summaryId in DB
        this.setState({summaryId:0})
    }

    activateButton(e) {
        let element = e.target.innerHTML;
        switch(element) {
            case 'Entry':
                this.setState(prevState => ({
                    date: prevState.date,
                    entryStatus:'active',
                    summaryStatus: '',
                    lessonStatus: '',
                    goalStatus: ''
                }));
                break;
            case 'Summary':
                this.setState(prevState => ({
                    date: prevState.date,
                    entryStatus:'',
                    summaryStatus: 'active',
                    lessonStatus: '',
                    goalStatus: ''
                }));
                break;
            case 'Lesson':
                this.setState(prevState => ({
                    date: prevState.date,
                    entryStatus:'',
                    summaryStatus: '',
                    lessonStatus: 'active',
                    goalStatus: ''
                }));
                break;
            case 'Goals':
                this.setState(prevState => ({
                    date: prevState.date,
                    entryStatus:'',
                    summaryStatus: '',
                    lessonStatus: '',
                    goalStatus: 'active'
                }));
                break;
            default:
                break;
        }

    }

    render(){
        return (
            <Router>
            <div className="container-fluid row">
                <div>{this.props.selectedDate + ''}</div>
                <div className="list-group list-group-horizontal">
                    <Link to="/DailyEntry" className={`list-group-item list-group-item-action ${this.state.entryStatus}`} onClick={this.activateButton}>Entry</Link>
                    <Link to="/Summary" className={`list-group-item list-group-item-action ${this.state.summaryStatus}`} onClick={this.activateButton}>Summary</Link>
                    <Link to="/Lessons" className={`list-group-item list-group-item-action ${this.state.lessonStatus}`} onClick={this.activateButton}>Lesson</Link>
                    <Link to="/SetGoals" className={`list-group-item list-group-item-action ${this.state.goalStatus}`} onClick={this.activateButton}>Goals</Link>
                </div>
                <div className="container-fluid row" style={{border:'solid'}}>
                    <Route 
                        path="/DailyEntry" 
                        render={(props) => <InputEntry {...props} reloadEntryView={this.props.reloadEntryView} /> }
                        //component={InputEntry} 
                    />
                    <Route 
                        path="/SetGoals" 
                        component={InputGoals} 
                    />
                    <Route 
                        path="/Lessons" 
                        render={(props) => <InputLesson {...props} summary_Id={this.state.summaryId} /> }
                       /*component={InputLesson} */
                    />
                    <Route 
                        path="/Summary" 
                        component={InputSummary} 
                    />
                </div>
            </div> 
            </Router>
        );
    }
}

export default InputElement;